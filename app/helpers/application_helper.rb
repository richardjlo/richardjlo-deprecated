module ApplicationHelper

	# Returns the full title on a per-page basis.
	def full_title(page_title)
		base_title = "Richard Lo"		
		if page_title.empty?
					base_title
		else			
			"#{base_title} | #{page_title}"
		end
	end

	#highlights current page in navbar
	def active_class(page_title, cur_tab)
		active_class = "active"
		if page_title == cur_tab 
			active_class
		end
	end
		

end
